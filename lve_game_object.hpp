#ifndef game_object_hpp_
#define game_object_hpp_
#include "lve_model.hpp"
#include <memory>


namespace lve {
struct Transform2dComponent
{
    // position: up,down,left,right
    glm::vec2 translation{};
    glm::vec2 scale{1.f, 1.f};
    /* rotation mx:
     * ^i               ^j
     * |cos teta        -sin teta|
     * |sin teta        cos teta |
     *
     * SOH, CAH, TOA
     * sin t = opp /h
     * cos t = adj /h
     * tan t = opp /adj
     */
    float rotation;

    // scale *column* matrix as: ^i goes from (1, 0) .:. (scalex, 0)
    //                           ^j goes from (0, 1) .:. (0, scaley)
    glm::mat2 mat2() const
    {
        const float s = glm::sin(rotation);
        const float c = glm::cos(rotation);
        glm::mat2 rotationMatrix = {{c, s}, {-s, c}};
        glm::mat2 scaleMatrix{{scale.x, .0f}, {.0f, scale.y}};
        return rotationMatrix * scaleMatrix ;
    }
};

/* brief
 * This class is a collection of game objects and methods.
 * For example in Mario: The warp pipe, shrooms, player character.
 * At first It servs also a base concept, and may contain methods that
 * won't be needed for derives.
 * Note: It is inefficent, but maintains great flexibility, and fast to implement
 * Later: entity & system approach
 */
class LveGameObject
{
public:
    using id_t = unsigned int;

    static LveGameObject createGameObject()
    {
        static id_t currentId = 0;
        return LveGameObject{currentId++};
    }

    id_t getId() const { return id; }
    std::shared_ptr<LveModel> model{};
    glm::vec3 color{};
    Transform2dComponent transform2d{};

    LveGameObject(const LveGameObject&) = delete;
    LveGameObject& operator=(const LveGameObject&) = delete;
    LveGameObject(LveGameObject&&) = default;
    LveGameObject& operator=(LveGameObject&&) = default;

private:
    LveGameObject(id_t objId) : id{objId} {}
    id_t id;
};


} //namespace lve
    
#endif //game_object_hpp_
