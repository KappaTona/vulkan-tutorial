#include "simple_render_system.hpp"

// don't use degrees
#define GLM_FORCE_RADIANS
// Vulkan: use 0 to 1, OpenGL: -1 to 1 as of Z axis is different
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <memory>
#include <stdexcept>


namespace lve {

/*
 * Used for Vulkan Push Constants
 * VPS good for limited and frequently changed
 * shader datas.
 * Member order is important as of must be the same
 * as in shaders/simple_shader.vert layout(push_constant).
 * alignas(16) needed for shader layout uniforms padding.
 */
struct SimplePushConstantData
{
    // identity matrix: if i=j: transform[i][j] = 1;
    glm::mat2 transform{1.f};
    glm::vec2 offset;
    alignas(16) glm::vec3 color;
};

SimpleRenderSystem::SimpleRenderSystem(LveDevice& device, VkRenderPass renderPass)
    : lveDevice{device}
{
    createPipelineLayout();
    createPipeline(renderPass);
}

SimpleRenderSystem::~SimpleRenderSystem()
{
    vkDestroyPipelineLayout(lveDevice.device(), pipelineLayout, nullptr);
}

void SimpleRenderSystem::createPipelineLayout()
{
    VkPushConstantRange pushConstantRange{};
    pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(SimplePushConstantData);

    VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 0;
    pipelineLayoutInfo.pSetLayouts = nullptr;
    pipelineLayoutInfo.pushConstantRangeCount = 1;
    pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;

    if (vkCreatePipelineLayout(lveDevice.device(), &pipelineLayoutInfo, nullptr, &pipelineLayout)
        != VK_SUCCESS)
    {
        throw std::runtime_error("failed to create pipeline layout\n");
    }
}







void SimpleRenderSystem::renderGameObjects(VkCommandBuffer commandBuffer,
    std::vector<LveGameObject>& gameObjects)
{
    // update
    int i = 1;
    for (auto& obj: gameObjects)
    {
        i += 1;
        obj.transform2d.rotation =
            glm::mod<float>(obj.transform2d.rotation + .001f * i,
            2.f * glm::pi<float>());
    }

    lvePipeline->bind(commandBuffer);
    for (auto& obj: gameObjects)
    {
        // rotating triangle in circle
        obj.transform2d.rotation = glm::mod(obj.transform2d.rotation + .01f, glm::two_pi<float>());

        SimplePushConstantData push{};
        push.offset = obj.transform2d.translation;
        push.color = obj.color;
        push.transform = obj.transform2d.mat2();

        vkCmdPushConstants(commandBuffer, pipelineLayout,
             VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
             0, sizeof(SimplePushConstantData), &push);
        obj.model->bind(commandBuffer);
        obj.model->draw(commandBuffer);

    }
}



void SimpleRenderSystem::createPipeline(VkRenderPass renderPass)
{
    if (!pipelineLayout)
    {
        std::runtime_error("SimpleRenderSystem::createPipeline;\n"
            "Cannot create pipeline before pipeline layout\n");
    }

    PipelineConfigInfo pipelineConfig{};
    LvePipeline::defaultPipelineConfigInfo(pipelineConfig);
    pipelineConfig.renderPass = renderPass;
    pipelineConfig.pipelineLayout = pipelineLayout;
    lvePipeline = std::make_unique<LvePipeline>(lveDevice,
        "shaders/simple_shader.vert.spv", "shaders/simple_shader.frag.spv",
        pipelineConfig);
}



} //namespace lve
