#include "first_app.hpp"
#include "simple_render_system.hpp"
#include <vulkan/vulkan_core.h>

// don't use degrees
#define GLM_FORCE_RADIANS
// Vulkan: use 0 to 1, OpenGL: -1 to 1 as of Z axis is different
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

#include <memory>
#include <stdexcept>


namespace lve {

FirstApp::FirstApp()
{
    loadGameObjects();
}

FirstApp::~FirstApp()
{
}

void FirstApp::run()
{
    SimpleRenderSystem simpleRenderSystem{lveDevice, lveRenderer.getSwapChainRenderPass()};
    while (!lveWindow.shouldClose())
    {
        /* side-note:
         * this blocks the rendering. Noticable when resizing
         * details in docs, ofc.
         */
        glfwPollEvents();
        /*
         * We might be wondering why the {begin,end}SwapChainRenderpass and {begin,end}Frame 
         * haven't been combined into one single function.
         * We want the application to control these.
         * So for later integrating we can combine multiple renderpasses
         * for reflection or shadow or postprocesisng effects etc...
         */
        if (auto commandBuffer = lveRenderer.beginFrame())
        {
            lveRenderer.beginSwapChainRenderPass(commandBuffer);
            simpleRenderSystem.renderGameObjects(commandBuffer, gameObjects);
            lveRenderer.endSwapChainRenderPass(commandBuffer);
            lveRenderer.endFrame();
        }
    }

    vkDeviceWaitIdle(lveDevice.device());
}

void FirstApp::loadGameObjects()
{
    std::vector<LveModel::Vertex> verticies = {
        {{.0f, -.5f}, {1.f, .0f, .0f}},
        {{.5f, .5f}, {.0f, 1.0f, .0f}},
        {{-.5f, .5f}, {.0f, .0f, 1.0f}}
    };

    auto lveModel = std::make_shared<LveModel>(lveDevice, verticies);
    auto triangle = LveGameObject::createGameObject();
    triangle.model = lveModel;
    triangle.color = {.1f, .8f, .1f};
    triangle.transform2d.translation.x = .2f;
    triangle.transform2d.scale = {2.f, .5f};
    triangle.transform2d.rotation = .25f * glm::two_pi<float>();

    gameObjects.emplace_back(std::move(triangle));
}

} //namespace lve
