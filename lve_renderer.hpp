#ifndef lve_renderer_hpp_
#define lve_renderer_hpp_


#include "lve_device.hpp"
#include "lve_model.hpp"
#include "lve_swap_chain.hpp"
#include "lve_window.hpp"

#include <memory>
#include <stdexcept>

namespace lve {
class LveRenderer {
public:
    LveRenderer(LveWindow& window, LveDevice& device);
    ~LveRenderer();

    LveRenderer(const LveRenderer&) = delete;
    LveRenderer& operator=(const LveRenderer&) = delete;

    VkCommandBuffer beginFrame();
    void endFrame();
    void beginSwapChainRenderPass(VkCommandBuffer commandBuffer);
    void endSwapChainRenderPass(VkCommandBuffer commandBuffer);

    bool isFrameInProgress() const { return isFrameStarted; }
    VkCommandBuffer getCurrentCommandBuffer() const 
    {
        if (!isFrameStarted)
            std::runtime_error("LveRenderer::getCurrentCommandBuffer(); Cannot get command buffer when frame not in progress\n");
        return commandBuffers[currentFrameIndex];
    }

    VkRenderPass getSwapChainRenderPass() const { return lveSwapChain->getRenderPass(); }
    int getFrameImageIndex() const
    {
        if (!isFrameStarted)
            throw std::runtime_error("LveRenderer::getCurrentCommandBuffer(); Cannot get frame index when frame not in progress\n");

        return currentFrameIndex;
    }

private:
    void createCommandBuffers();
    void freeCommandBuffers();
    void recreateSwapChain();

    LveWindow& lveWindow;
    LveDevice& lveDevice;
    std::unique_ptr<LveSwapChain> lveSwapChain;
    std::vector<VkCommandBuffer> commandBuffers;

    uint32_t currentImageIndex;
    int currentFrameIndex{0};
    bool isFrameStarted = false;
};

} //namespace lve

#endif //lve_renderer_hpp_
