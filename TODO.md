[on the fly thoughts]
- LvePipeline file locations should be separated constans
- fix member namings from camle to snake
- remove extra Lve since lve is a namespace already
- `LvePipeline` private functions can be anon-namespace funcs in cpp

- review `lveSwapChain` const member functions
- review `lveSwapChain` private member functions
- `static void LveWindow::framebufferReszeCallback(GLFWwindow*, int, int);`
    - probably can be a lambda
- when using uniforms in shaders aligment matters! tutorial 09.

[Recommendations]
- [Simple VK allocator](http://kylehalladay.com/blog/tutorial/2017/12/13/Custom-Allocators-Vulkan.html)
- video creator [git](https://github.com/blurrypiano/littleVulkanEngine)
- [official vulkan samples](https://github.com/KhronosGroup/Vulkan-Samples)
- [Colorspace and format explaination](https://docs.google.com/document/d/1DsppK1HviSpwJ_sg-npY9zQsOwH-VPGQyvYRh5_eKXA/edit)
- [Renderpass-compabilities](https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/chap8.html#renderpass-compatibility)
- [tutorial 9](https://github.com/blurrypiano/littleVulkanEngine/tree/master/littleVulkanEngine/tutorial09)
    - [linalg](https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)
- [Componenet Based Architecture in Games -- tut 10](https://www.raywenderlich.com/2806-introduction-to-component-based-architecture-in-games)
    - [Major colomn order](https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/geometry/row-major-vs-column-major-vector)
    - Game object runtime models, entity based system (ECS/Data oriented)
    - Object orinented (inheritance etc..)
    - [S.O question](https://gamedev.stackexchange.com/questions/31473/what-is-the-role-of-systems-in-a-component-based-entity-architecture)
- [Example of an Entity Manager implementation](https://austinmorlan.com/posts/entity_component_system/)

[Ideas]
- in video tutorial 10 and `optinal_task::tripangle` is a good concept for future "Entites" KEKW

[optional memo]
- Rainbow system and Gravity system example in video 11.
    - [rainbowsystem pastebin](https://pastebin.com/si4eaa3g)
    - [gravity pastebin](https://pastebin.com/7NCfdNrd)
