#include "lve_window.hpp"
#include <iostream>


namespace lve {
LveWindow::LveWindow(int w, int h, std::string name):
    width{w}, height{h}, window_name{name}
{
    initWindow();
}

LveWindow::~LveWindow()
{
    glfwDestroyWindow(window);
    glfwTerminate();
}

void LveWindow::initWindow()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window = glfwCreateWindow(width, height, window_name.c_str(), nullptr, nullptr);
    glfwSetWindowUserPointer(window, this);
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}


void LveWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface)
{
    if (glfwCreateWindowSurface(instance, window, nullptr, surface) != VK_SUCCESS)
        throw std::runtime_error("Failed to create window surface\n");
}

void LveWindow::framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
    auto lveWindow = reinterpret_cast<LveWindow*>(glfwGetWindowUserPointer(window));
    lveWindow->frameBufferResized = true;
    lveWindow->width = width;
    lveWindow->height = height;
}


}
