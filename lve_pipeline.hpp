#ifndef lve_pipeline_hpp_
#define  lve_pipeline_hpp_
#include "lve_device.hpp"

#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>


namespace lve {
struct PipelineConfigInfo {
    PipelineConfigInfo(const PipelineConfigInfo&) =delete;
    PipelineConfigInfo& operator=(const PipelineConfigInfo&) =delete;

    VkPipelineViewportStateCreateInfo viewportInfo;
    VkPipelineInputAssemblyStateCreateInfo inputAssemplyInfo;
    VkPipelineRasterizationStateCreateInfo rasterizationInfo;
    VkPipelineMultisampleStateCreateInfo multisampleInfo;
    VkPipelineColorBlendAttachmentState colorBlendAttachment;
    VkPipelineColorBlendStateCreateInfo colorBlendInfo;
    VkPipelineDepthStencilStateCreateInfo depthStencilInfo;
    VkPipelineLayout pipelineLayout = nullptr;
    VkRenderPass renderPass = nullptr;
    uint32_t subpass = 0;
    std::vector<VkDynamicState> dynimicStateEnables;
    VkPipelineDynamicStateCreateInfo dynamicStateInfo;
};
class LvePipeline
{
public:
    LvePipeline(LveDevice &device,
        const std::string& vertFilepath,
        const std::string& fragFilepath,
        const PipelineConfigInfo& configInfo);
    ~LvePipeline();

    LvePipeline(const LvePipeline&) = delete;
    LvePipeline& operator=(const LvePipeline&) = delete;

    void bind(VkCommandBuffer commandBuffer);

    static void defaultPipelineConfigInfo(PipelineConfigInfo& configInfo);

private:
    static std::vector<char> readFile(const std::string& filepath);

    void createGraphicsPipeline(const LveDevice &device,
        const std::string& vertFilepath,
        const std::string& fragFilepath,
        const PipelineConfigInfo& configInfo);

    void createShaderModule(const std::vector<char>& code, VkShaderModule *shaderModule);
    // TODO: aggregation, this will outlive the class.
    LveDevice &lveDevice;

    VkPipeline graphicsPipeline;
    VkShaderModule vertShaderModule;
    VkShaderModule fragShaderModule;
};


}
#endif // lve_pipeline_hpp_
