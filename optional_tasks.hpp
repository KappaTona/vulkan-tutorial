#ifndef optional_tasks_hpp_
#define  optional_tasks_hpp_
#include <glm/fwd.hpp>
#include <vector>
#include "lve_model.hpp"
#include "lve_game_object.hpp"


namespace optional {
// tutorial 8?
void sierpinski_triangle(
    std::vector<lve::LveModel::Vertex> &verticies,
    int depth,
    glm::vec2 left,
    glm::vec2 right,
    glm::vec2 top);

// tutorial 10
void tripingle(std::vector<lve::LveGameObject>& gameObjects,
    std::shared_ptr<lve::LveModel> lveModel);
}

#endif  //optional_tasks_hpp_
