#ifndef swap_chain_hpp_
#define swap_chain_hpp_
#include "lve_device.hpp"
#include <memory>
#include <vulkan/vulkan.h>


namespace lve {
/*
 * Handles the syncronazition of double or triple buffering
 * depending on hardware capabilities
 */
class LveSwapChain
{
public:
    // two command buffers
    static constexpr int MAX_FRAMES_IN_FLIGHT = 2;

    LveSwapChain(LveDevice& deviceRef, VkExtent2D extent);
    LveSwapChain(LveDevice& deviceRef, VkExtent2D extent, std::shared_ptr<LveSwapChain> preveious);
    ~LveSwapChain();

    LveSwapChain(const LveSwapChain&) = delete;
    LveSwapChain& operator=(const LveSwapChain&) = delete;

    VkFramebuffer getFrameBuffer(int index);
    VkRenderPass getRenderPass();
    VkImageView getImageView(int index);
    VkFormat getSwapChainImageFormat();
    VkExtent2D getSwapChainExtent();
    VkFormat findDepthFormat();
    VkResult acquireNextImage(uint32_t *imageIndex);
    VkResult submitCommandBuffers(const VkCommandBuffer *buffers, uint32_t *imageIndex);

    size_t imageCount();
    uint32_t width();
    uint32_t height();
    float extentAspectRatio();
    bool compareSwapFormats(const LveSwapChain& swapChain) const
    {
        return swapChain.swapChainDepthFormat == swapChainDepthFormat
            && swapChain.swapChainImageFormat == swapChainImageFormat;
    }

private:
    void init();
    void createSwapChain();
    void createImageViews();
    void createDepthResources();
    void createRenderPass();
    void createFramebuffers();
    void createSyncObjects();

    // Helper functions
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(
        const std::vector<VkSurfaceFormatKHR>& availableFormats);
    VkPresentModeKHR chooseSwapPresentMode(
        const std::vector<VkPresentModeKHR>& availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

    LveDevice& device;

    VkFormat swapChainImageFormat;
    VkFormat swapChainDepthFormat;
    VkExtent2D swapChainExtent;
    VkExtent2D windowExtent;
    VkRenderPass renderPass;

    VkSwapchainKHR swapChain;
    std::shared_ptr<LveSwapChain> oldSwapChain;

    /*
     * Framebuffer objects, handling color and depth
     */
    std::vector<VkFramebuffer> swapChainFramebuffers;
    std::vector<VkImage> depthImages;
    std::vector<VkDeviceMemory> depthImageMemories;
    std::vector<VkImageView> depthImageViews;
    std::vector<VkImage> swapChainImages;
    std::vector<VkImageView> swapChainImageViews;

    std::vector<VkSemaphore> imageAvailableSemaphores;
    std::vector<VkSemaphore> renderFinishedSemaphore;
    std::vector<VkFence> inFlightFences;
    std::vector<VkFence> imagesInFlight;
    size_t currentFrame = 0;
};


} //namespace lve
#endif //swap_chain_hpp_
