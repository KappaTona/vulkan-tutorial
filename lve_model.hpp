#ifndef lve_model_hpp_
#define  lve_model_hpp_
#include "lve_device.hpp"

// don't use degrees
#define GLM_FORCE_RADIANS
// Vulkan: use 0 to 1, OpenGL: -1 to 1 as of Z axis is different
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>


namespace lve {
/*
 * purpose of this class to
 * be able to take vertex data, allocate memory
 * and copy the data to device GPU, to render.
 */
class LveModel
{
public:
    struct Vertex {
        glm::vec2 position;
        glm::vec3 color;

        static std::vector<VkVertexInputBindingDescription> getBindingDesriptions();
        static std::vector<VkVertexInputAttributeDescription> getAttributeDesriptions();
    };

    LveModel(LveDevice& device, const std::vector<Vertex>& verticies);
    ~LveModel();
    LveModel(const LveModel&) = delete;
    LveModel& operator=(const LveModel&) = delete;

    void bind(VkCommandBuffer commandBuffer);
    void draw(VkCommandBuffer commandBuffer);

private:
    void createVertexBuffers(const std::vector<Vertex>& verticies);

    LveDevice& lveDevice;

    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    uint32_t vertexCount;
};
}
#endif // lve_model_hpp_
