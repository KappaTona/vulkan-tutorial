#include "lve_renderer.hpp"
#include <vulkan/vulkan_core.h>
#include <memory>
#include <stdexcept>


namespace lve {

LveRenderer::LveRenderer(LveWindow& window, LveDevice& device)
    : lveWindow{window}, lveDevice{device}
{
    recreateSwapChain();
    createCommandBuffers();
}

LveRenderer::~LveRenderer()
{
    freeCommandBuffers();
}

VkCommandBuffer LveRenderer::beginFrame()
{
    if (isFrameStarted)
        std::runtime_error("LveRenderer::beginFrame();\n"
            "Can't call begin frame while already in progress\n");

    auto result = lveSwapChain->acquireNextImage(&currentImageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        recreateSwapChain();
        // frame not succesfully started
        return nullptr;
    }
    // later: can occur when window is resized
    if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
        throw std::runtime_error("LveRenderer::beginFrame();Failed to acquire next image\n");

    isFrameStarted = true;

    auto commandBuffer = getCurrentCommandBuffer();
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

    if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
    {
        std::runtime_error("LveRenderer::beginFrame(); Failed to begin recording command buffer\n");
    }

    return commandBuffer;
}

void LveRenderer::endFrame()
{
    if (!isFrameStarted)
        throw std::runtime_error("LveRenderer::endFrame();"
            "Can't call endFrame while frame is not in progress\n");

    const auto commandBuffer = getCurrentCommandBuffer();
    if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
        std::runtime_error("LveRenderer::endFrame(); Failed to record command buffer!\n");

    auto result = lveSwapChain->submitCommandBuffers(&commandBuffer, &currentImageIndex);
    if (result == VK_ERROR_OUT_OF_DATE_KHR
        || result == VK_SUBOPTIMAL_KHR
        || lveWindow.wasWindowResized())
    {
        lveWindow.resetWindowResizedFlag();
        recreateSwapChain();
    } else if (result != VK_SUCCESS)
        throw std::runtime_error("LveRenderer::endFrame();\n"
            "failed to present swap chain image\n");

    isFrameStarted = false;
    currentFrameIndex = (currentFrameIndex + 1) % LveSwapChain::MAX_FRAMES_IN_FLIGHT;
}

void LveRenderer::beginSwapChainRenderPass(VkCommandBuffer commandBuffer)
{
    if (!isFrameStarted)
        throw std::runtime_error("LveRenderer::beginSwapChainRenderPass();\n"
            "Can't call endFrame while frame is not in progress\n");

    if (commandBuffer != getCurrentCommandBuffer())
        throw std::runtime_error("LveRenderer::beginSwapChainRenderPass();\n"
            "Can't begin render pass from a different frame\n");

    VkRenderPassBeginInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderPassInfo.renderPass = lveSwapChain->getRenderPass();
    renderPassInfo.framebuffer = lveSwapChain->getFrameBuffer(currentImageIndex);
    renderPassInfo.renderArea.offset = {0, 0};
    renderPassInfo.renderArea.extent = lveSwapChain->getSwapChainExtent();

    std::array<VkClearValue, 2> clearValues{};
    clearValues[0].color = {.01f,.01f,.01f,.1f,};
    clearValues[1].depthStencil = {1.f, 0};
    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.data();

    // inline for primay buffers
    vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    VkViewport viewport{};
    viewport.x = .0f;
    viewport.y = .0f;
    viewport.width = static_cast<float>(lveSwapChain->getSwapChainExtent().width);
    viewport.height = static_cast<float>(lveSwapChain->getSwapChainExtent().height);
    viewport.minDepth = .0f;
    viewport.maxDepth = 1.f;
    VkRect2D scissor{{0, 0}, lveSwapChain->getSwapChainExtent()};
    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);
}

void LveRenderer::endSwapChainRenderPass(VkCommandBuffer commandBuffer)
{
    if (!isFrameStarted)
        throw std::runtime_error("LveRenderer::endSwapChainRenderPass();\n"
            "Can't call endFrame while frame is not in progress\n");

    if (commandBuffer != getCurrentCommandBuffer())
        throw std::runtime_error("LveRenderer::endSwapChainRenderPass();\n"
            "Can't begin render pass from a different frame\n");

    vkCmdEndRenderPass(commandBuffer);
}

void LveRenderer::createCommandBuffers()
{
    commandBuffers.resize(LveSwapChain::MAX_FRAMES_IN_FLIGHT);

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    // primary: submit for executation queue, can't be called from other
    // secondary: can't be submited, but can be called from other
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = lveDevice.getCommandPool();
    allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());

    if (vkAllocateCommandBuffers(lveDevice.device(), &allocInfo, commandBuffers.data())
        != VK_SUCCESS)
    {
        throw std::runtime_error("failed to allocate command buffer\n");
    }

}

void LveRenderer::freeCommandBuffers()
{
    vkFreeCommandBuffers(lveDevice.device(), lveDevice.getCommandPool(),
        static_cast<uint32_t>(commandBuffers.size()),
        commandBuffers.data());
    commandBuffers.clear();
}









void LveRenderer::recreateSwapChain()
{
    auto extent = lveWindow.getExtent();
    // this can occour for example minimalization
    while (extent.width == 0 || extent.height == 0)
    {
        extent = lveWindow.getExtent();
        glfwWaitEvents();
    }

    // wait until current swapchain is not used
    vkDeviceWaitIdle(lveDevice.device());

    // curious if the branch can be reversed. aka if (lveSwapChain)
    if (!lveSwapChain)
    {
        // ensure that old swap chain is destroyed before next line
        lveSwapChain = nullptr;
        lveSwapChain = std::make_unique<LveSwapChain>(lveDevice, extent);
    } else 
    {
        std::shared_ptr<LveSwapChain> oldSwapchain = std::move(lveSwapChain);
        lveSwapChain = nullptr;
        lveSwapChain = std::make_unique<LveSwapChain>(lveDevice, extent, oldSwapchain);

        if (!oldSwapchain->compareSwapFormats(*lveSwapChain.get()))
            throw std::runtime_error("LveRenderer::recreateSwapChain();\n"
                "Swap chain image(or depth) format has changed!\n");
    }

    // if render pass compatible: don't recreate pipleilne
    // details in renderpass-compatibilities
}



} //namespace lve
