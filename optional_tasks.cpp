#include "optional_tasks.hpp"
#include "lve_model.hpp"
#include <memory>
#include <glm/gtc/constants.hpp>


namespace optional {
/* usage:
 * in FirstApp::loadModels();
 *  std::vector<LveModel::Vertex> verticies{};
 *  optional::sierpinski_triangle(verticies, 5,
 *      {.0f, -.5f}, {.5f, .5f}, {-.5f, .5f});
 *  lveModel = std::make_unique<LveModel>(lveDevice, verticies);
*/
void sierpinski_triangle(
    std::vector<lve::LveModel::Vertex> &vertices,
    int depth,
    glm::vec2 left,
    glm::vec2 right,
    glm::vec2 top)
{
    if (depth <= 0) {
        vertices.push_back({top});
        vertices.push_back({right});
        vertices.push_back({left});
    } else {

    auto leftTop = .5f * (left + top);
    auto rightTop = .5f * (right + top);
    auto leftRight = .5f * (left + right);

    sierpinski_triangle(vertices, depth - 1, left, leftRight, leftTop);
    sierpinski_triangle(vertices, depth - 1, leftRight, right, rightTop);
    sierpinski_triangle(vertices, depth - 1, leftTop, rightTop, top);
    }
}



void tripingle(std::vector<lve::LveGameObject>& gameObjects,
    std::shared_ptr<lve::LveModel> lveModel)
{
  // https://www.color-hex.com/color-palette/5361
  std::vector<glm::vec3> colors{
      {1.f, .7f, .73f},
      {1.f, .87f, .73f},
      {1.f, 1.f, .73f},
      {.73f, 1.f, .8f},
      {.73, .88f, 1.f}  //
  };
  for (auto& color : colors) {
    color = glm::pow(color, glm::vec3{2.2f});
  }
  for (int i = 0; i < 40; i++) {
    auto triangle = lve::LveGameObject::createGameObject();
    triangle.model = lveModel;
    triangle.transform2d.scale = glm::vec2(.5f) + i * 0.025f;
    triangle.transform2d.rotation = i * glm::pi<float>() * .025f;
    triangle.color = colors[i % colors.size()];
    gameObjects.push_back(std::move(triangle));
  }
}
}
