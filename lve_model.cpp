#include "lve_model.hpp"

#include <stdexcept>
#include <cstring>
#include <vulkan/vulkan_core.h>


namespace lve {
LveModel::LveModel(LveDevice& device, const std::vector<Vertex>& verticies) : lveDevice{device}
{
    createVertexBuffers(verticies);
}
LveModel::~LveModel()
{
    vkDestroyBuffer(lveDevice.device(), vertexBuffer, nullptr);
    vkFreeMemory(lveDevice.device(), vertexBufferMemory, nullptr);
}

void LveModel::createVertexBuffers(const std::vector<Vertex>& verticies)
{
    vertexCount = static_cast<uint32_t>(verticies.size());
    if (vertexCount < 3)
        throw std::runtime_error("Error in: LveModel::createVertexBuffers verticies.size() < 3!\n");

    VkDeviceSize bufferSize = sizeof(verticies[0]) * vertexCount;
    // host = CPU
    lveDevice.createBuffer(bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        vertexBuffer, vertexBufferMemory);

    void* data;
    vkMapMemory(lveDevice.device(), vertexBufferMemory, 0, bufferSize, 0, &data);
    // ::data() is const, and memcpy doesn't care iguess
    std::memcpy(data, verticies.data(), static_cast<size_t>(bufferSize));
    vkUnmapMemory(lveDevice.device(), vertexBufferMemory);
}

void LveModel::bind(VkCommandBuffer commandBuffer)
{
    VkBuffer buffers [] = {vertexBuffer};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, buffers, offsets);
}

void LveModel::draw(VkCommandBuffer commandBuffer)
{
    vkCmdDraw(commandBuffer, vertexCount, 1, 0, 0);
}

std::vector<VkVertexInputBindingDescription> LveModel::Vertex::getBindingDesriptions()
{
    std::vector<VkVertexInputBindingDescription> bindingDescription(1);
    bindingDescription[0].binding = 0;
    bindingDescription[0].stride = sizeof(Vertex);
    bindingDescription[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    return bindingDescription;
}


std::vector<VkVertexInputAttributeDescription> LveModel::Vertex::getAttributeDesriptions()
{
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions(2);
    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].offset = offsetof(Vertex, position);
    attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1; // location in vertex shader number must match
    attributeDescriptions[1].offset = offsetof(Vertex, color);
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;

    return attributeDescriptions;
}


}
